public class GCDLoop{
	public static void main(String[] args){
		int number1 = Integer.parseInt(args[0]);
		int number2 = Integer.parseInt(args[1]);
	

			while (number2 > 0){
				int remainder = number1 % number2;
				number1 = number2;
				number2 = remainder;
				
			}
		
			System.out.println(number1);
	
	}
}