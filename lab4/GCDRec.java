public class GCDRec{
	public static void main(String[] args){
		int value1 = Integer.parseInt(args[0]);
		int value2 = Integer.parseInt(args[1]);
		
		System.out.println(GCD(value1, value2));
	
	}

	public static long GCD(long number1, long number2){
		
		if (number2 == 0){
			return number1;}
		else
			return GCD(number2, number1 % number2);
	}
}

